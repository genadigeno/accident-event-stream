## Applikationsbeskrivning
Event stream applikationen läser och distribuerar de inkommande olyckshändelserna till specifika 'sink topics' i realtid,</br>
sådana är `emergency.events`, `police.events`, `fire-rescue.evens` och så vidare... Om något fel uppstår händelseström</br>
applikationen skickar det till 'dead letter topic' (i fall av standardinställningar: `accident.events-dlt`) med värdefullt </br>tillägg
information och fortsätter processen för andra. Det ger möjlighet att övervaka de korrupta händelserna för en</br>
applikation för händelsekälla. Fördelningsmekanism beror på en olyckstyp, till exempel om olyckstypen är</br>
brottsligt kommer det att skickas till räddningstjänst och polis, men om det är en brandolycka då - polis, </br>
räddningstjänst och brandräddnings 'service'.
### Visuell
### Accident Event Stream applikationsarkitektur
![alt text](./materials/Event-Stream-App.png)

## Komma igång
### Installationskrav
För att starta programmet måste du ha installerat:
- Docker på din maskin 
- git  
  eller
- Apache kafka kluster med minst tre mäklare
- git
- maven 3.9.x
- JDK-17

### Installationssteg
Projektinstallationen kan göras med docker-compose.yml via command line interface (CMD):
```
git clone https://gitlab.com/genadigeno/accident-event-stream.git ams-event-stream
cd ams-event-stream
docker compose up
```
eller
```
git clone https://gitlab.com/genadigeno/accident-event-stream.git ams-event-stream
cd ams-event-stream
mvn clean package
java java -jar ./target/accident-event-stream.jar
```
### JVM Parameters
- `SERVER_PORT` - applikationsportnummer, standard: 8080
- `FIRE_RESCUE_SERVICE_TOPIC_NAME` - kafka topic namn för brandräddningstjänst, standard: fire-rescue.events
- `STATISTICS_SERVICE_TOPIC_NAME` - kafka topic namn för statistics service, standard: statistics.events
- `POLICE_SERVICE_TOPIC_NAME` - kafka topic namn för police service, standard: police.events
- `EMERGENCY_SERVICE_TOPIC_NAME` - kafka topic namn för emergency service, standard: emergency.events
- `SOURCE_TOPIC_NAME` - kafka source topic namn för event stream application, standard: accident.events
- `DLT_SOURCE_TOPIC_NAME` - kafka dead letter namn för for event stream application, standard: accident.events-dlt
- `BOOTSTRAP_SERVERS` - kafka kluster url, standard: localhost:9092,localhost:9093
- `SCHEMA_REGISTRY_URL` - schema registry url, standard: http://localhost:8081
- `AVRO_SUBJECT_VERSION` - subject version av avro serde, standard: latest
- `CACHE_MAX_SIZE_BUFFERING` - buffer storlek av statefull operationer, standard: 0
- `STATE_DIRECTORY` - state store directory, standard: /state-dir
#### Example `java -DSERVER_PORT=9898 -DBOOTSTRAP_SERVERS=localhost:9092 -jar ./target/accident-event-stream.jar`
***
### Använd teknologier
- <img src="./materials/eda.png" height="15" alt="img"> Event-Driven Architecture
- <img src="./materials/java.png" height="15" alt="img"> Java 17
- <img src="./materials/boot.png" height="15" alt="img"> Spring Boot & Spring Boot JPA
- <img src="./materials/kafka.png" height="15" alt="img"> Apache Kafka & Kafka Streams bibliotek
- <img src="./materials/docker.png" height="15" alt="img"> Docker
- <img src="./materials/git.png" height="15" alt="img"> Git
- <img src="./materials/gitlab-ci-cd.png" height="15" alt="img"> Gitlab CI/CD
### Projektstatus
Avslutad
***
 <img src="./materials/uk.png" height="20" alt="img"> 

[Click here to read the description in English](README.md)
