## Application Description
Event stream application reads and distributes the incoming accident events to specific sink topics in real-time,</br>
such are `emergency.events`, `police.events`, `fire-rescue.evens` and so on... If some error arises event stream </br>
application sends it to dead letter topic(in case of default settings: `accident.events-dlt`) with valuable additional </br>
information and continues process for other ones. That provides possibility of monitoring of the corrupted events for an</br>
event source application. Distribution mechanism depends on an accident type, for example if the accident type is</br>
criminal it will be sent to emergency and police services, however if it is fire accident then - police, emergency and </br>
fire-rescue service.
### Visual
### Accident Event Stream application architecture
![alt text](./materials/Event-Stream-App.png)

## Getting started
### Installation requirements
In order to start up the application you need have installed:
- Docker on you machine </br>
- git </br>
or
- Apache kafka cluster with at least three brokers
- git
- maven 3.9.x 
- JDK-17

### Installation steps
The project installation could be done using docker-compose.yml via command line interface (CMD):
```
git clone https://gitlab.com/genadigeno/accident-event-stream.git ams-event-stream &&
cd ams-event-stream &&
docker compose up
```
or 
```
git clone https://gitlab.com/genadigeno/accident-event-stream.git ams-event-stream &&
cd ams-event-stream &&
mvn clean package &&
java -jar ./target/accident-event-stream.jar
```
### JVM Parameters
- `SERVER_PORT` - application port number, default: 8080
- `FIRE_RESCUE_SERVICE_TOPIC_NAME` - kafka topic name for fire-rescue service, default: fire-rescue.events
- `STATISTICS_SERVICE_TOPIC_NAME` - kafka topic name for statistics service, default: statistics.events
- `POLICE_SERVICE_TOPIC_NAME` - kafka topic name for police service, default: police.events
- `EMERGENCY_SERVICE_TOPIC_NAME` - kafka topic name for emergency service, default: emergency.events
- `SOURCE_TOPIC_NAME` - kafka source topic name for event stream application, default: accident.events
- `DLT_SOURCE_TOPIC_NAME` - kafka dead letter topic name for event stream application, default: accident.events-dlt
- `BOOTSTRAP_SERVERS` - kafka cluster url, default: localhost:9092,localhost:9093
- `SCHEMA_REGISTRY_URL` - schema registry url, default: http://localhost:8081
- `AVRO_SUBJECT_VERSION` - subject version of avro serde, default: latest
- `CACHE_MAX_SIZE_BUFFERING` - buffer size of statefull operations, default: 0
- `STATE_DIRECTORY` - state store directory, default: ./state-dir
#### Example `java -DSERVER_PORT=9898 -DBOOTSTRAP_SERVERS=localhost:9092 -jar ./target/accident-event-stream.jar`

***
### Used Technologies
- <img src="./materials/eda.png" height="15" alt="img"> Event-Driven Architecture
- <img src="./materials/java.png" height="15" alt="img"> Java 17
- <img src="./materials/boot.png" height="15" alt="img"> Spring Boot & Spring Boot JPA
- <img src="./materials/kafka.png" height="15" alt="img"> Apache Kafka & Kafka Streams library
- <img src="./materials/docker.png" height="15" alt="img"> Docker
- <img src="./materials/git.png" height="15" alt="img"> Git
- <img src="./materials/gitlab-ci-cd.png" height="15" alt="img"> Gitlab CI/CD
### Project status
Completed
***
 <img src="./materials/se.png" height="20" alt="img"> 

[Klicka här för att läsa beskrivningen på svenska](README-SE.md)
