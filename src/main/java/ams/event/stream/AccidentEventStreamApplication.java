package ams.event.stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccidentEventStreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccidentEventStreamApplication.class, args);
	}
}
