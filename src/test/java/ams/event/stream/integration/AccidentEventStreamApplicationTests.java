package ams.event.stream.integration;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
class AccidentEventStreamApplicationTests {

	@Test
	void contextLoads() {
	}

}
