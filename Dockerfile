FROM openjdk:17-alpine

RUN mkdir "app"
WORKDIR /app

RUN mkdir "logs"
VOLUME $PWD/logs

RUN mkdir "state-dir"

COPY ./target/accident-event-stream.jar .

ENV SERVER_PORT=8080
ENV FIRE_RESCUE_SERVICE_TOPIC_NAME="fire-rescue.events"
ENV STATISTICS_SERVICE_TOPIC_NAME="statistics.events"
ENV POLICE_SERVICE_TOPIC_NAME="police.events"
ENV EMERGENCY_SERVICE_TOPIC_NAME="emergency.events"
ENV SOURCE_TOPIC_NAME="accident.events"
ENV DLT_SOURCE_TOPIC_NAME="accident.events-dlt"
ENV BOOTSTRAP_SERVERS="localhost:9092,localhost:9093"
ENV SCHEMA_REGISTRY_URL="http://localhost:8081"
ENV AVRO_SUBJECT_VERSION="latest"
ENV CACHE_MAX_SIZE_BUFFERING="0"
ENV STATE_DIRECTORY=$PWD/state-dir

EXPOSE ${SERVER_PORT}

ENTRYPOINT ["java", "-jar"]

CMD ["accident-event-stream.jar"]